// CONTROLLER
app.controller('HomeCtrl', ['$scope','$state','$stateParams','$http', function($scope,$state,$stateParams,$http){

  var estadoActual 	= $state.current.name;
  $scope.logo 		= "Sitio web JS";
  console.log("state :: ", $state.current.name);
  // MOCK
  $scope.menu 	= [
  	{"name":"Home","link":"#/home","state":"home","active":false},
  	{"name":"About","link":"#/about","state":"about","active":false},
  	{"name":"Services","link":"#services","state":"services","active":false},
  	{"name":"Contact","link":"#contact","state":"contact","active":false}
  ];
  $scope.products 	= [];

  $scope.categorias = [
  	{"name":"Autos","link":"#autos"},
  	{"name":"Motos","link":"#motos"}
  ];

  $scope.sliders 	= [
  	{"name":"Slide 1","src":"http://placehold.it/900x350"},
  	{"name":"Slide 2","src":"http://placehold.it/900x350"}
  ]


  angular.forEach( $scope.menu, function (item) {
  	if(item.state == estadoActual){
  		item.active = true;
  		return;
  	}
  });

  $http.get('api/products').then( function (result) {
  		$scope.products = result.data;
  })

  console.log("menu ::", $scope.menu);


}]);