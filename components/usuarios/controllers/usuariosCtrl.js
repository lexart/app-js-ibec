// CONTROLLER
app.controller('UsuariosCtrl', ['$scope','$state','$stateParams', function($scope, $state, $stateParams){

  $scope.txt    = "Hola " + $stateParams.nick + " - ID: " + $stateParams.id;
  $scope.user 	= {};
  $scope.usuarios = [];

  console.log("PARAM URL ::", $stateParams.id, $stateParams.nick, $stateParams.otros);

  $scope.guardar 	= function () {
  	console.log(" USER: ", $scope.user);
  	var user = angular.copy($scope.user);

  	$scope.usuarios.push(user);
  	$scope.user = {};
  }

  // ESTADO - USUARIOS
  if($state.includes('usuarios')){
  	console.log("USUARIOS OK");
  }

  if($state.includes('usuario_nuevo')){
  	console.log("USUARIOS NUEVO", $scope.usuarios);
  }

}]);