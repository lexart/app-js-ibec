<?php 

$products = array(
			array(
					"name" => "Item One",
					"img" => "http://placehold.it/700x400",
					"price" => 24.99,
					"currency" => "$",
					"description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!",
					"rate" => array(1,2,3,4)
				),
			array(
					"name" => "Item Two",
					"img" => "http://placehold.it/700x400",
					"price" => 24.99,
					"currency" => "USD",
					"description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!",
					"rate" => array(1,2,3,4)
				),
			array(
					"name" => "Item Three",
					"img" => "http://placehold.it/700x400",
					"price" => 24.99,
					"currency" => "EUR",
					"description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!",
					"rate" => array(1,2,3,4,5)
				)
		);

echo json_encode( $products );

?>