// ROUTEO
app.run(['$rootScope', '$state', '$stateParams',
  function ($rootScope,   $state,   $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }]);

app.config(['$httpProvider','$stateProvider','$urlRouterProvider',function ($httpProvider, $stateProvider, $urlRouterProvider) {

  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  $urlRouterProvider.otherwise("/home");

  // Now set up the states
  $stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "components/home/views/indexView.html",
      controller: "HomeCtrl"
    })
    .state('about', {
      url: "/about",
      templateUrl: "components/about/views/indexView.html",
      controller: "HomeCtrl"
    })
    .state('usuarios', {
      url: "/usuarios/:id/:nick/:otros",
      templateUrl: "components/usuarios/views/indexView.html",
      controller: "UsuariosCtrl"
    })
    .state('usuario_nuevo', {
      url: "/usuario/new",
      templateUrl: "components/usuarios/views/usuarioForm.html",
      controller: "UsuariosCtrl"
    })
}]);